<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/GameDAO.php');
require_once(dirname(__FILE__) . '/../../models/Game.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $idGame = $_POST["idGame"];
    $name = $_POST["name"];
    $description = $_POST["description"];
    $urlPicture = $_POST["urlPicture"];
    $price = $_POST["price"];
    $valoration = $_POST["valoration"];
    $empresa = $_POST["empresa"];
    

    $game = new Game();
    $game->setIdGame($idGame);
    $game->setName($name);
    $game->setDescription($description);
    $game->setUrlPicture($urlPicture);
    $game->setPrice($price);
    $game->setValoration($valoration);
    $game->setEmpresa($empresa);

    $gameDAO = new GameDAO();
    $gameDAO->update($game);

    header('Location: ../../../index.php');
}

?>

