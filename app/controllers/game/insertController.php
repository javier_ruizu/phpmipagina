<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/GameDAO.php');
require_once(dirname(__FILE__) . '/../../models/Game.php');
require_once(dirname(__FILE__) . '/../../models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    
    $name = ValidationsRules::test_input($_POST["name"]);
    $description = ValidationsRules::test_input($_POST["description"]);
    $urlPicture = ValidationsRules::test_input($_POST["urlPicture"]);
    $price = ValidationsRules::test_input($_POST["price"]);
    $valoration = ValidationsRules::test_input($_POST["valoration"]);
    $empresa = ValidationsRules::test_input($_POST["empresa"]);
    
    // Creación de objeto auxiliar   
    $game = new Game();
    $game->setName($_POST["name"]);
    $game->setDescription($_POST["description"]);
    $game->setUrlPicture($_POST["urlPicture"]);
    $game->setPrice($_POST["price"]);
    $game->setValoration($_POST["valoration"]);
    $game->setEmpresa($_POST["empresa"]);

    //Creamos un objeto GameDAO para hacer las llamadas a la BD
    $gameDAO = new GameDAO();
    $gameDAO->insert($game);
    
    header('Location: ../../../index.php');
    
}
?>

