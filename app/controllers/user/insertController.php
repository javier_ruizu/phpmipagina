<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../persistence/DAO/UserDAO.php');
require_once(dirname(__FILE__) . '/../../../utils/SessionUtils.php');
require_once(dirname(__FILE__) . '/../../models/User.php');
require_once(dirname(__FILE__) . '/../../models/validations/ValidationsRules.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    
    $email = ValidationsRules::test_input($_POST["email"]);
    $password = ValidationsRules::test_input($_POST["password"]);
    
    $user = new User();
    $user->setEmail($_POST["email"]);
    $user->setPassword($_POST["password"]);
        
    //Recogemos el email y password
    //$email = $_POST["email"];
    //$password = $_POST["password"];


    //Creamos un objeto UserDAO para hacer las llamadas a la BD
    $userDAO = new UserDAO();
    $userDAO->insert($user);
    
    // Establecemos la sesión
        SessionUtils::setSession($user);
        SessionUtils::startSessionIfNotStarted();
        
        header('Location: ../../private/views/index.php');    
  

         
 
}
?>

