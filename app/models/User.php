<?php

class User{

    //Atributos
    private $iduser;
    private $email;
    private $password;


    //Getters
    public function getIdUser() {
        return $this->userid;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function getPassword() {
        return $this->password;
    }
    
    //Setters
    public function setIdUser($iduser) {
        $this->iduser = $iduser;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
}

?>
