<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../../../persistence/DAO/GameDAO.php');
require_once(dirname(__FILE__) . '/../../../models/Game.php');
//Compruebo que me llega por GET el parámetro
if (isset($_GET["idGame"])) {
    $idGame = $_GET["idGame"];
    //Creamos un objeto BookDAO para hacer las llamadas a la BD
    $gameDAO = new GameDAO();
    $game = $gameDAO->selectById($idGame);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Gestión de Videojuegos</title>
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../../../../index.php"><img src="assets/img/small-logo.png" alt="" ></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a  class="nav-link " href="insert.php">Agregar un videojuego</a>
                    </li>
                </ul>
            </div>  
        </nav>
        <!-- PageContent -->
        <div class="container">

            <div class="card" >
                <img class="card-img-top rounded mx-auto d-block avatar" src='<?php echo $game->getUrlPicture() ?>' alt="Card image cap">
                <div class="card-block">
                    <h1 class="card-title"> <?php echo $game->getName() ?></h1>
                    <h3 class="card-subtitle mb-2 text-success"> <?php echo $game->getPrice() ?> €</h3>
                    <h5 class="card-subtitle mb-2 text-secondary">Valoracion: <?php echo $game->getValoration() ?></h5>
                    <p class=" card-text description"> <?php echo $game->getDescription() ?></p>
                    <p class="card-text"><small class="text-muted">Made by: <?php echo $game->getEmpresa() ?> </small></p>
                </div>
                <div  class=" btn-group card-footer" role="group">
                    <a type="button" class="btn btn-success" href="edit.php?idGame=<?php echo $game->getIdGame() ?>">Modificar</a> 
                    <a type="button" class="btn btn-danger" href="../../controllers/game/deleteController.php?idGame=<?php echo $game->getIdGame() ?>">Borrar</a> 
                </div>
            </div>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; A. F. 2017</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /.container -->
        <!-- Java Script Boostrap-->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" ></script>
    </body>

</html>


