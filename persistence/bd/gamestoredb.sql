CREATE DATABASE IF NOT EXISTS gamestore;

USE gamestore;

CREATE TABLE IF NOT EXISTS games (
	idGame int not null AUTO_INCREMENT,
	name varchar(255),
	description text,
	urlPicture varchar(255),
        price decimal(4,2),
        valoration decimal(3,2),
        empresa text,

	PRIMARY KEY( idGame )
);

CREATE TABLE IF NOT EXISTS users (
	iduser int not null AUTO_INCREMENT,
	email varchar(255),
	password varchar(255),

	PRIMARY KEY( id )
);

INSERT INTO `games` VALUES (1,'Fifa 21','Un juego de fútbol que saca lo peor de la gente', 'https://images-na.ssl-images-amazon.com/images/I/81mBk8%2BsxrL._AC_SX569_.jpg', 69,95, 7.5, 'EA Sports');