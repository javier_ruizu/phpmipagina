<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class GameDAO {

    //Se define una constante con el nombre de la tabla
    const GAMES_TABLE = 'games';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . GameDAO::GAMES_TABLE;
        $result = mysqli_query($this->conn, $query);
        $games = array();
        while ($gameDB = mysqli_fetch_array($result)) {

            $game = new Game();
            $game->setIdGame($gameDB["idGame"]);
            $game->setName($gameDB["name"]);
            $game->setDescription($gameDB["description"]);
            $game->setUrlPicture($gameDB["urlPicture"]);
            $game->setPrice($gameDB["price"]);
            $game->setValoration($gameDB["valoration"]);
            $game->setEmpresa($gameDB["empresa"]);
            
            array_push($games, $game);
        }
        return $games;
    }

    public function insert($game) {
        $query = "INSERT INTO " . GameDAO::GAMES_TABLE .
                " (name, description, urlPicture, price, valoration, empresa) VALUES(?,?,?,?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $game->getName();
        $description = $game->getDescription();
        $urlPicture = $game->getUrlPicture();
        $price = $game->getPrice();
        $valoration = $game->getValoration();
        $empresa = $game->getEmpresa();
        
        mysqli_stmt_bind_param($stmt, 'sssdds', $name, $description, $urlPicture, $price, $valoration, $empresa);
        return $stmt->execute();
    }

    public function selectById($idGame) {
        $query = "SELECT name, description, urlPicture, price, valoration, empresa FROM " . GameDAO::GAMES_TABLE . " WHERE idGame=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $idGame);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $description, $urlPicture, $price, $valoration, $empresa);

        $game = new Game();
        while (mysqli_stmt_fetch($stmt)) {
            $game->setIdGame($idGame);
            $game->setName($name);
            $game->setDescription($description);
            $game->setUrlPicture($urlPicture);
            $game->setPrice($price);
            $game->setValoration($valoration);
            $game->setEmpresa($empresa);
            
       }

        return $game;
    }

    public function update($game) {
        $query = "UPDATE " . GameDAO::GAMES_TABLE .
                " SET name=?, description=?, urlPicture=?, price=?, valoration=?, empresa=?"
                . " WHERE idGame=?";
        $stmt = mysqli_prepare($this->conn, $query);
        
        $name = $game->getName();
        $description= $game->getDescription();
        $urlPicture = $game->getUrlPicture();
        $price = $game->getPrice();
        $valoration = $game->getValoration();
        $empresa = $game->getEmpresa();
        
        $idGame = $game->getIdGame();
        mysqli_stmt_bind_param($stmt, 'sssddsi', $name, $description, $urlPicture, $price, $valoration, $empresa, $idGame);
        return $stmt->execute();
    }
    
    public function delete($idGame) {
        $query = "DELETE FROM " . GameDAO::GAMES_TABLE . " WHERE idGame=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $idGame);
        return $stmt->execute();
    }

        
}

?>
