<?php

/* Clase UserDAO.php
 * Clase que aplica el Patrón de Diseño DAO para manejar toda la información de un objeto Usuario.
 * author Eugenia Pérez
 * mailto eugenia_perez@cuatrovientos.org
 */
require_once(dirname(__FILE__) . '/../../persistence/conf/PersistentManager.php');

class UserDAO {

    //Se define una constante con el nombre de la tabla
    const USER_TABLE = 'users';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }
    
    public function selectAll() {
        $query = "SELECT * FROM " . UserDAO::USER_TABLE;
        $result = mysqli_query($this->conn, $query);
        $users= array();
        while ($userBD = mysqli_fetch_array($result)) {

            $user = new User();
            $user->setIdUser($userBD["iduser"]);
            $user->setEmail($userBD["email"]);
            $user->setPassword($userBD["password"]);
            
            array_push($users, $user);
        }
        return $users;
    }
    
    /* Función para chequear el usruario */
    
    public function check($user) {
        $query = "SELECT email, password FROM " . UserDAO::USER_TABLE . " WHERE email=? AND password=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $auxEmail = $user->getEmail();
        $auxPass =  $user->getPassword();
                 
        mysqli_stmt_bind_param($stmt, 'ss', $auxEmail, $auxPass);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt); 
        $rows = mysqli_stmt_num_rows($stmt);
        if($rows>0)         
            return true;
        else
            return false;
    }
    
    
    public function selectById($id) {
        $query = "SELECT email, password FROM " . UserDAO::USER_TABLE . " WHERE iduser=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $email, $password);

        $user = new User();
        while (mysqli_stmt_fetch($stmt)) {
            $user->setIdUser($id);
            $user->setEmail($email);
            $user->setPassword($password);
       }

        return $user;
    }
    
    
    public function update($user) {
        $query = "UPDATE " . UserDAO::USER_TABLE .
                " SET email=?, password=?"
                . " WHERE iduser=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $email = $user->getEmail();
        $password= $user->getPassword();
        $id = $user->getIdUser();
        mysqli_stmt_bind_param($stmt, 'ssi', $email, $password, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . UserDAO::USER_TABLE . " WHERE iduser =?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

    /* Función de login de usuario */

    public function login($email, $password) {
        $query = "SELECT * FROM " . UserDAO::USER_TABLE .
                " WHERE email='" . $email . "' AND password='" .
                $password . "'";
        $result = mysqli_query($this->conn, $query);

        if ($result && mysqli_num_rows($result) > 0) {
            $userBD = mysqli_fetch_array($result);
         
            $user = new User();
            $user->setIdUser($userBD['iduser']);
            $user->setEmail($userBD['email']);
            $user->setPassword($userBD['password']);

            return $user;
        } 
        return null;
    }

    /*
     * Inserta un usuario en la base de datos. Como la clave primaria es 
     * autogenerada, no es necesario insertarla.
     * Es importante que a los valores a insertar que pongas dentro del 
     * paréntesis VALUES, los encierres en comillas simples.
     */

    public function insert($user) {
        /*$query = "INSERT INTO " . UserDAO::USER_TABLE .
                " (email, password) VALUES('" . $email . "', '" .
                $password . "')";
        
        mysqli_query($this->conn, $query);*/
        
        $query = "INSERT INTO " . UserDAO::USER_TABLE . " (email, password) VALUES(?, ?)";
        $stmt = mysqli_prepare($this->conn, $query);
        
        $email = $user->getEmail();
        $password = $user->getPassword();
        
        mysqli_stmt_bind_param($stmt, 'ss', $email, $password);
        return $stmt->execute();
      
        
    }

}

?>
